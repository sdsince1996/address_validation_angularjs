
var app = angular.module("AddressValidation", ['ngRoute']);

// -----------------------Routing----------------------

app.config(["$routeProvider", function($routeProvider) {
    $routeProvider.
        when("/Address", {
            templateUrl: "Component/Address.html",
            controller: "addressCtrl"
        }).
        otherwise({
            redirectTo: "/Address"
        });
}]);