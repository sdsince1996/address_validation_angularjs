app.controller("addressCtrl", function ($scope, $http) {
 
    $scope.Save = function (address, pincode, city, district, state, country) {
        var data = {
            address: address,
            pinCode: Number(pincode),
            city: city,
            district: district,
            state: state,
            country: country
        }
        if (address == null || pincode == null || city == null || district == null || state == null || country == null)
        {
            document.getElementById("errorMsg").innerHTML = "All fields are required."
        }
        else if(address.length > 165)
        {
            document.getElementById("errorMsg").innerHTML = "Address should not be more than 165 characters."
        }
        else
        {
            $http.post("https://localhost:7118/api/Address/Add", JSON.stringify(data))
            .then(function (response) {
                console.log(response);
                alert("Address added.");
                $scope.data = response.data.data;
                $scope.getAll();

            }, function (error) {
                alert("cannot add address.")
                console.log(error)
            })
        }
        
    };

    $scope.getAll = function () {
        $http.get("https://localhost:7118/api/Address/Get")
        .then(function (response){
            console.log(response)
            $scope.allAddresses = response.data.data
        }, function (error){
            console.log(error)
        })
    };
    $scope.getByID = function(id) {
        $http.get(`https://localhost:7118/api/Address/GetByID?addressId=${id}`)
        .then(function (response){
            console.log(response)
            $scope.data = response.data.data
        }, function (error){
            console.log(error)
        })
    }
})